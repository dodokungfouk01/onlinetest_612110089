﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmoteControllerPreview : MonoBehaviour
{
    private float waitTime=5f;
    private bool canEmotes=true;
    [SerializeField] GameObject Emote_Angly;
    [SerializeField] GameObject Emote_Happy;


    private void Update()
    {
        //Angly
        if(Input.GetKey(KeyCode.Keypad0)&&canEmotes==true)
        {
            Emote_Angly.SetActive(true);
            canEmotes=false;
        }
        if(Emote_Angly.activeInHierarchy==true)
        {
            TimeUpdate();
            
            

        }
         //Happy
        if(Input.GetKey(KeyCode.Keypad1)&&canEmotes==true)
        {
            Emote_Happy.SetActive(true);
            canEmotes=false;
        }
        if(Emote_Happy.activeInHierarchy==true){
            TimeUpdate();
           
        }
   

        //////////////////////////////////////////////
        if(waitTime<=1)
            {
                endemotes(); 
                Debug.Log("CanEmote");          
                
            }
      
       

        
    }
    private void endemotes()
    {
        Emote_Angly.SetActive(false);
        Emote_Happy.SetActive(false);
       
        canEmotes=true;
        waitTime=5;
    }
    private void TimeUpdate()
    {
        if(waitTime>=0){
            waitTime-=Time.deltaTime;
            Debug.Log("W8 CD");
        }
        else waitTime=0;
        
    }
}
