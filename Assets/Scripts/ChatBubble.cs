﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ChatBubble : MonoBehaviour
{    [SerializeField]SpriteRenderer backgroundSpriteRenderer;
     [SerializeField]SpriteRenderer iconSpriteRenderer;
     [SerializeField]TextMeshPro textMeshPro;
    
    private void awake(){
       // backgroundSpriteRenderer=transform.Find("BackGround").GetComponent<SpriteRenderer>();
       // iconSpriteRenderer=transform.Find("Icon").GetComponent<SpriteRenderer>();
        //textMeshPro=GameObject.Find("Text11").GetComponent<TextMeshPro>();
       
        
    }
    private void Start()
    {
        Setup("Kuay");
    }
    private void Setup(string text){
        textMeshPro.SetText(text);
        textMeshPro.ForceMeshUpdate();
        Vector2 padding =new Vector2(4f,2f);
        Vector2 textSize = textMeshPro.GetRenderedValues(false);
        backgroundSpriteRenderer.size = textSize+padding;
    }
}
