﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class Disconnect : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisconnectPlayer()
    {
        StartCoroutine(DiscoonectAndLoad());
    }
    IEnumerator DiscoonectAndLoad()
    {
        PhotonNetwork.Disconnect();
        
        //PhotonNetwork.LeaveRoom();
        while(PhotonNetwork.InRoom);
            yield return null;
        
        SceneManager.LoadSceneAsync(1);
        SceneManager.LoadScene(0);
    }
}
