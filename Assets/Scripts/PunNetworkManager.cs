﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;

public class PunNetworkManager : ConnectAndJoinRandom 
{
    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    bool isGameStart = false;
    bool isFirstSetting = false;
    //public GameObject HealingPrefeb;
    public int numberOfHealing = 5;
    float m_count = 0;
    public float m_CountDownDropHeal = 10;

    private void Awake()
    {
        singleton = this;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        
        Debug.Log("New Player. " + newPlayer.ToString());
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Camera.main.gameObject.SetActive(false);

        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }

        //PhotonNetwork.CurrentRoom.CustomProperties
        //PhotonNetwork.CurrentRoom.Players[0].CustomProperties
    }

    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, 
                                    new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        //SkinLoader playerSkin = GamePlayerPrefab.GetComponent<SkinLoader>();
        //playerSkin.UpdateSprite();
        
        isGameStart = true;
    }

    private void Update() {
        if(PhotonNetwork.IsMasterClient != true)
            return;
        if (isGameStart == true){
            
        }
    }
    
}
