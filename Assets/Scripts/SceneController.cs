﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;


public class SceneController : MonoBehaviourPun
{
    public void Play()
    {
        SceneManager.LoadScene(1);
        PhotonNetwork.ConnectUsingSettings();
        
        
    }
    
}
