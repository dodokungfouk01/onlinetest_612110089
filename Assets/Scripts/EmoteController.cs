using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class EmoteController : MonoBehaviourPun {

    [SerializeField] GameObject Emote_Angly;
    [SerializeField] GameObject Emote_Happy;
    [SerializeField] GameObject Emote_Shy;
    [SerializeField] GameObject Emote_Creepy;
    [SerializeField] GameObject Emote_Oops;
    
    private float waitTime = 5;
    private bool canEmotes = true;

    private void Update () {
        if (photonView.IsMine) {

            //Angly

            if (Input.GetKey (KeyCode.Keypad0) && canEmotes == true) {

                photonView.RPC ("showemoteAngly", RpcTarget.All);

                canEmotes = false;

            }
            if (Emote_Angly.activeInHierarchy == true) {
                TimeUpdate ();


            }
            //Happy
            if(Input.GetKey(KeyCode.Keypad1)&&canEmotes==true){
                photonView.RPC("showemoteHappy",RpcTarget.All);
                canEmotes=false;
            }
            if(Emote_Happy.activeInHierarchy==true){
                TimeUpdate();
            }
            //Shy
            if(Input.GetKey(KeyCode.Keypad2)&&canEmotes==true){
                photonView.RPC("showemoteShy",RpcTarget.All);
                canEmotes=false;
            }
            if(Emote_Shy.activeInHierarchy==true){
                TimeUpdate();
            }
            //Creepy
            if(Input.GetKey(KeyCode.Keypad3)&&canEmotes==true){
                photonView.RPC("showemoteCreepy",RpcTarget.All);
                canEmotes=false;
            }
            if(Emote_Creepy.activeInHierarchy==true){
                TimeUpdate();
            }
            //Oops
            if(Input.GetKey(KeyCode.Keypad4)&&canEmotes==true){
                photonView.RPC("showemoteOops",RpcTarget.All);
                canEmotes=false;
            }
            if(Emote_Oops.activeInHierarchy==true){
                TimeUpdate();
            }


            //Cooldown time
            if (waitTime == 0) {
                photonView.RPC ("endemote", RpcTarget.All);

            }
        }
    }

    [PunRPC]
    public void showemoteAngly () {
        Emote_Angly.SetActive (true);
    }
    [PunRPC]
    public void showemoteHappy () {
        Emote_Happy.SetActive (true);
    }
    [PunRPC]
    public void showemoteShy(){
        Emote_Shy.SetActive(true);
    }
    [PunRPC]
    public void showemoteCreepy(){
        Emote_Creepy.SetActive(true);
    }
    [PunRPC]
    public void showemoteOops(){
        Emote_Oops.SetActive(true);
    }


    [PunRPC]
    public void endemote () { //Add emote for disable
        Emote_Angly.SetActive (false);
        Emote_Happy.SetActive(false);
        Emote_Shy.SetActive(false);
        Emote_Creepy.SetActive(false);
        Emote_Oops.SetActive(false);
        canEmotes = true;
        waitTime = 5;
    }

    private void TimeUpdate () {
        if (waitTime >= 0) {
            waitTime -= Time.deltaTime;
        } else waitTime = 0;

    }

}