using System;
using UnityEngine;
using UnityEngine.Serialization;
using Photon.Pun;
using Photon.Pun.Demo.Procedural;

public class CharacterController : MonoBehaviourPun
{
    
    [SerializeField] private float speed = 2f;

    [SerializeField] private Rigidbody rb = null;
    
    
    public bool unbalance = false;
    
    
   
    
   
    

    
    
    private Vector3 _movement;
    
    #region MonoBehaviour
    private void Update()
    {
        if (!photonView.IsMine)
            return;
        
        if (this.rb.velocity.x > speed || this.rb.velocity.z > speed ||
            this.rb.velocity.x < -speed || this.rb.velocity.z < -speed)
        {
            unbalance = true;
        }
        else
        {
            unbalance = false;
        }
        /*animator.SetBool (WALK_PROPERTY,
          Math.Abs (_movement.sqrMagnitude) > Mathf.Epsilon);*/
        ProcessInput();
    
    }

    private void ProcessInput()
    {
        //Get Input
        float inputX = 0;
        float inputY = 0;

        inputX = Input.GetAxisRaw("Horizontal");
        inputY = Input.GetAxisRaw("Vertical");
        // Normalize
        _movement = new Vector3(inputX, 0, inputY).normalized;

        
    }

    private void FixedUpdate()
    {
        if (!unbalance)
            Move();
    }
    private void Move()
    {
        rb.velocity = new Vector3(_movement.x * speed, rb.velocity.y,_movement.z * speed);
    }

    #endregion
}